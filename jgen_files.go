package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

type jgenFiles struct {
	dir        string
	userScript string
	job        string
	output     string
	status     string
	jobsub     string
	jobdel     string
	jobst      string
}

func createFiles(userCode string, jgenResources *JgenResources) jgenFiles {
	var f jgenFiles
	f.dir = makeTempdir()

	outputFile := createOutputFile(f.dir)
	statusFile := createStatusFile(f.dir)
	userScript := f.dir + "/user_script.sh"
	ioutil.WriteFile(userScript, []byte(userCode), 0755)

	userJob := generateUserJob(f.dir,
		userScript,
		outputFile,
		statusFile,
		jgenResources)

	f.output = outputFile
	f.status = statusFile
	f.userScript = userScript
	f.job = userJob

	return f
}

func makeTempdir() string {
	home := os.Getenv("HOME")
	dir, err := ioutil.TempDir(home, "jobrun_temp")
	if err != nil {
		log.Fatal(err)
	}
	return dir
}

func generateUserJob(dir string, userScript string, outputFile string, statusFile string, resources *JgenResources) string {
	userJob := fmt.Sprintf("%s\n%s >> %s 2>&1\necho $? > %s",
		resources.JgenHeader(),
		userScript,
		outputFile,
		statusFile)

	filename := dir + "/user_job.jgen"
	ioutil.WriteFile(filename, []byte(userJob), 0755)
	return filename
}

func createOutputFile(dir string) string {
	outputFile := dir + "/output.txt"
	os.Create(outputFile)
	return outputFile
}

func createStatusFile(dir string) string {
	statusFile := dir + "/status.txt"
	os.Create(statusFile)
	return statusFile
}

func (f *jgenFiles) getStatusFileContent() string {
	text, _ := ioutil.ReadFile(f.status)
	return string(text)
}
