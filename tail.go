package main

import (
	"bufio"
	"io"
	"log"
	"os"
	"time"
)

func tail(filename string, done *bool, out io.Writer) {

	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	rd := bufio.NewReader(f)
	for {
		line, err := rd.ReadString('\n')
		if err == io.EOF {
			if *done {
				break
			}
			time.Sleep(time.Second * 1)
		} else {
			out.Write([]byte(line))
		}

	}
}
