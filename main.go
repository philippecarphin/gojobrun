package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"
)

type programOptions struct {
	keepTmpFiles     bool
	hideCommands     bool
	tmpdir           string
	generateTmpFiles bool
	annotate         bool
	verbosity        int
}

func main() {
	yamlMain()
	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, syscall.SIGINT)

	r, po := parseCommandLine()

	// The outside of this module provides us with
	// the text of a script
	gitlabUserCode := getScriptFromGitlab(po.hideCommands)

	// We then wrap this code inside a JgenJob
	cmd := newExecutorCommand(gitlabUserCode, *r)

	waitCh := make(chan int)
	go func() {
		waitCh <- cmd.run()
	}()

	var jobStatus int
	select {
	case <-signalCh:
		cmd.AbortCh <- 8
		jobStatus = <-waitCh
	case jobStatus = <-waitCh:
	}

	os.Exit(jobStatus)
}

func parseCommandLine() (*JgenResources, *programOptions) {
	j := NewJgenResources()
	defaultImage := "eccc/eccc_all_ppp_ubuntu-18.04-amd64_latest"
	flag.StringVar(&j.Name, "name", "gojobrun_job", "Name of job")
	flag.BoolVar(&j.Joinouterr, "joinouterr", true, "Comgine stdout and stderr")
	flag.StringVar(&j.Outpath, "outpath", "~", "Path where output of job will be put")
	flag.IntVar(&j.Nslots, "nslots", 1, "Number of slots")
	flag.IntVar(&j.Ncores, "ncores", 7, "Number of cores")
	flag.StringVar(&j.Memory, "memory", "8G", "Quantity of memory of memory")
	flag.StringVar(&j.Tmpfs, "tmpfs", "4G", "Size of tmpfs")
	flag.StringVar(&j.Image, "image", defaultImage, "Image to run on")
	flag.IntVar(&j.Wallclock, "wallclock", 300, "Wallclock time limit for job")

	o := programOptions{}
	flag.BoolVar(&o.keepTmpFiles, "keep-tmp-files", false, "Do not cleanup temp files")
	flag.BoolVar(&o.hideCommands, "hide-commands", false, "Do not insert annotations")
	flag.StringVar(&o.tmpdir, "tmpdir", "asdf", "Directory where temp dir will be created")
	flag.BoolVar(&o.generateTmpFiles, "generate-tmp-files", false, "Only generate temporary files (don't submit anything)")
	flag.BoolVar(&o.annotate, "annotate", false, "Only produce annotated script from input script")
	flag.Parse()

	return j, &o
}

func logger(msg string) {
	log.Printf("\x1b[33m%s\x1b[0m", msg)
}

// TODO Signal handlers
// Find out how to do signal handlers in go
// Find an elegant way of reacting to a signal
// Add the stuff to delete a job
// TODO Ensure robust cleanup
// Look into how to do RAII in go.  The defer is
// not a "make sure it happens no matter what".
// TODO Add proper error handling
// Functions should propagate errors back to their
// caller if err != nil { print(...) return err }
// Read on proper error handling in go
// TODO Exit with exit code of USER SCRIPT
// TODO Documentation
