package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func getScriptFromGitlab(hideCommands bool) string {
	sb := strings.Builder{}
	rd := bufio.NewReader(os.Stdin)
	for {
		lineB, _, err := rd.ReadLine()
		if err == io.EOF {
			break
		}
		line := string(lineB)
		if hideCommands {
			sb.WriteString(line + "\n")
		} else {
			line := strings.TrimSpace(line)
			if strings.HasPrefix(line, "#") {
				sb.WriteString(echoComment(string(line)) + "\n")
			} else if line != "" {
				sb.WriteString(echoCommand(string(line)) + " ; " + string(line) + "\n")
			}
		}
	}
	return sb.String()
}

func escape(command string) string {
	s := strings.ReplaceAll(command, "\\", "\\\\")
	t := strings.ReplaceAll(s, "\"", "\\\"")
	return strings.ReplaceAll(t, "$", "\\$")
}

func echoCommand(command string) string {
	return fmt.Sprintf("echo \"\x1b[32;1m$ %s \x1b[0;m\"", escape(command))
}

func echoComment(command string) string {
	return fmt.Sprintf("echo \"\x1b[32;1m$\x1b[34m %s \x1b[0;m\"", command)
}
