package main

import (
	"log"
	"time"
)

func returnEight(c chan int) {
	defer close(c)
	log.Println("returnEight started")
	time.Sleep(time.Second * 5)
	log.Println("returnEight sending 8 on channel")
	c <- 8
}

func main() {

	cc := make(chan int)

	go returnEight(cc)
	log.Println("main started returnEight()")

	log.Println("main waiting on channel ...")
	eight := <-cc

	log.Printf("Received %d from the channel\n", eight)
	log.Println("Main done")
}
