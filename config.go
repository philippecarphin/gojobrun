package main

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"strings"
)

type jobrunConfig struct {
	OrdenvSetup string `yaml:"ORDENV_SETUP"`
	Defaults    JgenResources
}

func yamlMain() int {

	yml, _ := ioutil.ReadFile("config.yml")
	var config jobrunConfig
	yaml.Unmarshal(yml, &config)

	fmt.Printf("Name        : %s\n", config.Defaults.Name)
	fmt.Printf("Joinouterr  : %t\n", config.Defaults.Joinouterr)
	fmt.Printf("Outpath     : %s\n", config.Defaults.Outpath)
	fmt.Printf("Ncores      : %d\n", config.Defaults.Ncores)
	fmt.Printf("Chunks      : %d\n", config.Defaults.Chunks)
	fmt.Printf("Nslots      : %d\n", config.Defaults.Nslots)
	fmt.Printf("Wallclock   : %d\n", config.Defaults.Wallclock)
	fmt.Printf("Image       : %s\n", config.Defaults.Image)
	fmt.Printf("Tmpfs       : %s\n", config.Defaults.Tmpfs)
	fmt.Printf("Memory      : %s\n", config.Defaults.Memory)

	fmt.Printf("OrdenvSetup :\n   |%s\n", strings.ReplaceAll(strings.TrimSpace(config.OrdenvSetup), "\n", "\n   |"))

	fmt.Printf("Config : %v\n", config)
	fmt.Printf("Config : %#v\n", config)
	return 0

}
