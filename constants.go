package main

const ordenvSetup = `#!/bin/bash
unset ORDENV_SETUP
export ORDENV_SITE_PROFILE=20191220
export ORDENV_COMM_PROFILE=eccc/20200409
export ORDENV_GROUP_PROFILE=eccc/cmc/1.9.3
. /fs/ssm/main/env/ordenv-boot-20200204.sh >/dev/null 2>&1
export EC_ATOMIC_PROFILE_VERSION=1.11.0
`
