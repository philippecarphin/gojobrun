package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"
)

// The JgenJob Comment about my exported thing
type JgenJob struct {
	// Code that wants to be run in a job
	userCode string
	// Resources for that job (becomes the JGEN header)
	resources JgenResources

	// Struct wrapping various files used in the process
	// of submitting and monitoring the job
	files jgenFiles

	// Id of job once it has been sbumitted
	jobid string

	// Output streams of the job.  The output of the
	// job will be written to these streams when the
	// job runs.
	Stdout io.Writer
	Stderr io.Writer

	// State indicator for when the job is done
	done bool
	status int
}

// NewJgenJob Wraps userCode in a JgenJob object
func NewJgenJob(userCode string, resources JgenResources) *JgenJob {
	j := JgenJob{userCode: userCode, resources: resources}
	j.Stdout = os.Stdout
	j.Stderr = os.Stderr
	j.files = createFiles(j.userCode, &j.resources)
	return &j
}

// Start Start the job and tail goroutine
func (j *JgenJob) Start() {
	out, _ := submitWithJobsub(j.files.job)
	j.jobid = strings.TrimSpace(string(out))
	go tail(j.files.output, &j.done, j.Stdout)
}

// Wait for job to finish, checks at 2 second intervals
func (j *JgenJob) Wait() {
	for j.Check() == 0 && !j.done {
		time.Sleep(time.Second * 2)
	}
	// logger("Wait() Sleeping ...")
	// time.Sleep(time.Second * 6)
	// logger("Wait() Finished sleeping")
	// TODO Check status in file
	j.status = j.getJobStatus()
	j.done = true
}

// Check : Verify that the job has completed (return value is exit status of jobst.sh)
func (j *JgenJob) Check() int {
	jobstExitcode := checkWithJobst(j.jobid)
	if jobstExitcode != 0 {
		j.done = true
	}
	return jobstExitcode
}

// Delete : Stop a running job with jobdel
func (j *JgenJob) Delete() {
	deleteWithJobdel(j.jobid)
}

func (j *JgenJob) getJobStatus() (status int) {

	content := j.files.getStatusFileContent()
	nbRead, _ := fmt.Sscanf(content, "%d", &status)
	if nbRead != 1 {
		status = 77
	}
	// logger(fmt.Sprintf("getJobStatus() content='%s'", content))
	logger(fmt.Sprintf("getJobStatus() -> %d", status))
	return
}

func checkWithJobst(jobid string) int {
	jobstScript := fmt.Sprintf("set -eo pipefail\n%s\njobst -j %s | grep %s", ordenvSetup, jobid, jobid)
	cmd := exec.Command("/bin/bash", "-c", jobstScript)
	cmd.Stderr = os.Stderr
	_, err := cmd.Output()
	exitCode := cmd.ProcessState.ExitCode()
	if err != nil {
		exitCode = 1
	}
	msg := fmt.Sprintf("checkWithJobst -> %d", exitCode)
	logger(msg)
	return exitCode
}

func deleteWithJobdel(jobid string) string {
	logger("jgenJob : Deleting job with jobdel")
	jobdelScript := fmt.Sprintf("%s\njobdel %s", ordenvSetup, jobid)
	cmd := exec.Command("/bin/bash", "-c", jobdelScript)
	cmd.Stderr = os.Stderr
	out, _ := cmd.Output()
	return string(out)
}

func submitWithJobsub(filename string) ([]byte, error) {
	jobsubScript := fmt.Sprintf("%s\njobsub %s\nexec 1>/dev/null", ordenvSetup, filename)
	cmd := exec.Command("/bin/bash", "-c", jobsubScript)
	cmd.Stderr = os.Stderr
	out, err := cmd.Output()
	if err != nil {
		log.Fatal(err)
	}
	logger("submitWithJobsub -> " + strings.TrimSpace(string(out)))
	return out, err
}
