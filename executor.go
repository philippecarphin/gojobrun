package main

import (
	"os"
)

type executorCommand struct {
	buildScript string
	resources JgenResources
	JobStatus int
	AbortCh chan int
}

// NewExecutorCommand
func newExecutorCommand(buildScript string, resources JgenResources) *executorCommand {
	e := executorCommand{buildScript: buildScript, resources: resources}
	e.AbortCh = make(chan int)
	return &e
}

func (e *executorCommand) run () int {
	job := NewJgenJob(e.buildScript, e.resources)
	defer os.RemoveAll(job.files.dir)

	// We connect the output of the job to gitlab output stream
	job.Stderr = os.Stderr
	job.Stdout = os.Stdout

	// Runs jobsub
	// Tail starts in a goroutine after job is submitted
	// and begins writing to job.Stdout
	job.Start()

	jobStatusCh := make(chan int)
	go func() {
		job.Wait()
		jobStatusCh <- job.status
	}()

	var jobStatus int
	select {
	case jobStatus = <-jobStatusCh:
	case <-e.AbortCh:
		job.Delete()
		jobStatus = <-jobStatusCh
	}
	return jobStatus

}