package main

import (
	"fmt"
	"strings"
)

// JgenResources : Structure containing resource definitions for JGEN jobs
type JgenResources struct {
	Name       string `yaml:"job_name"`
	Image      string
	Outpath    string
	Memory     string
	Tmpfs      string
	Ncores     int
	Nslots     int
	Chunks     int
	Wallclock  int
	Joinouterr bool
}

// NewJgenResources : Return a JgenResources object initialized with default values
func NewJgenResources() *JgenResources {
	j := JgenResources{}
	j.Name = "gitlabjob.jgen"
	j.Joinouterr = true
	j.Outpath = "~"
	j.Ncores = 4
	j.Chunks = 1
	j.Nslots = 1
	j.Wallclock = 500
	j.Image = "eccc/eccc_all_ppp_ubuntu-18.04-amd64_latest"
	j.Tmpfs = "4G"
	j.Memory = "8G"
	return &j
}

// JgenHeader : Generate the JGEN header corresponding to the resources
func (j *JgenResources) JgenHeader() string {
	var sb strings.Builder

	fmt.Fprint(&sb, "#!/bin/bash\n")
	fmt.Fprintf(&sb, "#JGEN -r name=%s\n", j.Name)
	if j.Joinouterr {
		fmt.Fprint(&sb, "#JGEN -r joinouterr=y\n")
	}
	fmt.Fprintf(&sb, "#JGEN -r outpath=%s\n", j.Outpath)
	fmt.Fprintf(&sb, "#JGEN -c nslots=%d\n", j.Nslots)
	fmt.Fprintf(&sb, "#JGEN -c ncores=%d\n", j.Ncores)
	fmt.Fprintf(&sb, "#JGEN -c memory=%s\n", j.Memory)
	fmt.Fprintf(&sb, "#JGEN -c tmpfs=%s\n", j.Tmpfs)
	fmt.Fprintf(&sb, "#JGEN -c image=%s\n", j.Image)
	fmt.Fprintf(&sb, "#JGEN -r wallclock=%d\n", j.Wallclock)

	return sb.String()
}
